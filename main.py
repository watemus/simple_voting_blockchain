from flask import Flask, render_template, request
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, RadioField
from wtforms.validators import DataRequired
from blockchain_lib import *

app = Flask(__name__)
WTF_CSRF_SECRET_KEY = 'bla bla bla'


class CreateVotingForm(FlaskForm):
    question = StringField('Question', validators=[DataRequired()])
    options = StringField('Options')
    address = StringField('Address', validators=[DataRequired()])
    closedKey = PasswordField('Closed key', validators=[DataRequired()])


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/create_voting', methods=('GET', 'POST'))
def create_voting():
    form = CreateVotingForm(csrf_enabled=False)
    if form.validate_on_submit():
        # TODO:
        # чек адреса и закрытого ключа
        # добавление опроса в сеть
        question = request.form['question']
        opts = request.form['options']
        if opts[-1] != ';':
            opts += ';'
        options = request.form['options'].split(';')
        print(options)
        closed_key = request.form['closedKey']
        address = request.form['address']
        res = bl_create_voting(address, closed_key, question, options)
        return 'Voting created'
    return render_template('create_voting.html', form=form)


@app.route('/votings')
def votings():
    vtgs = []
    for i in range(bl_get_amount_votings()):
        if bl_get_voting_enabled(i):
            vtgs.append([bl_get_voting_question(i), '/vote/' + str(i)])
    return render_template('voting_list.html', voting_list=vtgs)


@app.route('/vote/<int:voting_id>', methods=('GET', 'POST'))
def vote(voting_id):
    options = bl_get_voting_variants(voting_id)
    question = bl_get_voting_question(voting_id)

    class VoteForm(FlaskForm):
        opts = RadioField(choices=options)
        address = StringField('Address', validators=[DataRequired()])
        closedKey = PasswordField('Closed key', validators=[DataRequired()])

    form = VoteForm(csrf_enabled=False)
    if form.validate_on_submit():
        chcd = -1
        for i in range(len(options)):
            if form.opts.data == options[i]:
                chcd = i
        if chcd != -1:
            address = request.form['address']
            closed_key = request.form['closedKey']
            bl_vote_voting(address, closed_key, voting_id, chcd)
            return f"You're voted"
    amount_voted = []
    for i in range(len(options)):
        amount_voted.append([options[i], bl_get_amount_votes(voting_id, i)])
    return render_template('vote.html', form=form, question=question, option=options, amount=amount_voted)
